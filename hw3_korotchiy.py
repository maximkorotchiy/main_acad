#------------------------------ex_2_START------------------------------
'''
import re
txt = 'Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.'
# txtList = txt.split()
txtList = re.findall(r"[\w']+", txt)
longestWord = 0
for i in range(1, len(txtList)):
    if len(txtList[longestWord]) < len(txtList[i]):
        longestWord = i
print(txtList[longestWord])

# ------------------------------ИЛИ------------------------------
import re
txt = 'Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.'
txtList = re.findall(r"[\w']+", txt)
print(max(txtList, key=len))
'''
#------------------------------ex_2_END------------------------------


#------------------------------ex_3.1_START------------------------------
'''
import re
txt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta.'
txtRev = txt[::-1]
print(txtRev)
'''
#------------------------------ex_3.1_END------------------------------

#------------------------------ex_3.2_START------------------------------
'''
import re
txt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta.'
# txtList = txt.split()[::-1]
txtList = re.findall(r"[\w']+", txt)[::-1]
txtListJoin = ' '.join(txtList)
print(txtListJoin)
'''
#------------------------------ex_3.2_END------------------------------
