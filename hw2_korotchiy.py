# -----------------------ex_1_START-----------------------
'''
def my_func(in_list = [1,2,3]):
    out_list = [el ** 2 for el in in_list]
    return out_list
print(my_func())

def my_func(in_list = [1,2,3]):
    out_list = [el ** 2 for el in in_list]
    out_tuple = tuple(out_list)
    return out_tuple
print(my_func())
'''
# -----------------------ex_1_END-----------------------

# -----------------------ex_2_START-----------------------
'''
def symmetry(str = input('Введите строку: ')):
    rev_str = str[::-1]
    if str == rev_str:
        return(bool(1))
    else:
        return(bool(0))
print(symmetry())
'''
# -----------------------ex_2_END-----------------------

# -----------------------ex_3_START-----------------------
'''
print('Введите 5 чисел: ')
list = [int(input()) for i in range(5)]
dict = {}
for i in list:
    dict.update({i : not bool(i%5)})
print(dict)
'''
# -----------------------ex_3_END-----------------------

# -----------------------ex_4_START-----------------------
'''
a = [3,7,12,7]
c = len(a)
if c % 2 != 0:
    a1 = [x for x in a if x % 2]
    print(a1)
else:
    a2 = [x for x in a if not x % 2]
    print(a2)
'''
# -----------------------ex_4_END-----------------------

# -----------------------ex_5_START-----------------------
'''
a = [1,4,8,6,3,7,1]
odd_list = sorted([x for x in a if x % 2])
even_list = sorted([x for x in a if not x % 2], reverse = True)
res = odd_list + even_list
print(res)
'''
# -----------------------ex_5_END-----------------------

# -----------------------ex_6_START---------ВЫПОЛНЕНО НЕ ПОЛНОСТЬЮ--------------
'''
dict = {'a': 1, 3: [1,5], 'e': 'abc', '6': []}
new_dict = {}
for i in dict:
    new_dict.update({type(dict[i]).__name__ : dict[i]})
print(new_dict)
'''
# -----------------------ex_6_END-----------------------

# -----------------------ex_9_START-----------------------
'''
input_list = [[1],[4,8],[6,3,7],[1,3]]
output_list = [i for sub_list in input_list for i in sub_list]
print(output_list)
'''
# -----------------------ex_9_END-----------------------
