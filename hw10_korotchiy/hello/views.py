from django.http import HttpResponse
from django.shortcuts import render

from hello.forms import QuadraticEquation, Square

from math import *

def first_run(request):
    if request.POST:
        context = {}
        form1 = QuadraticEquation(request.POST, prefix='form1')
        form2 = Square(request.POST, prefix='form2')
        if not form1.is_valid():
            context.update({'form1': form1})
        else:
            a = form1.cleaned_data['a']
            b = form1.cleaned_data['b']
            c = form1.cleaned_data['c']

            D = b**2 - 4*a*c
            x = 0
            x1 = 0
            x2 = 0
            no_x = 0

            if D > 0:
                x1 = (-b + sqrt(D))/(2*a)
                x2 = (-b - sqrt(D))/(2*a)
            elif D == 0:
                x = -b/(2*a)
            else:
                no_x = 'Корней нет'
            context.update({'form1': form1, 'success': True, 'D': D, 'x': x, 'x1': x1, 'x2': x2, 'a': a, 'b': b, 'c': c, 'no_x': no_x})

        if not form2.is_valid():
            context.update({'form2': form2})
        else:
            number = form2.cleaned_data['number']
            power = form2.cleaned_data['power']

            if power != None:
                result = pow(number, power)
            else:
                power = 1
                result = number
                # ИЛИ:
                # result = pow(number, power)
            context.update({'form2': form2, 'success': True, 'number': number, 'power': power, 'result':result})
        return render(request, 'index.html', context)
    return render(request, 'index.html', {'form1': QuadraticEquation(prefix='form1'), 'form2': Square(prefix='form2')})
