import requests
from bs4 import BeautifulSoup as bs

url = 'https://www.work.ua/jobs-django/'

def parser(url):
    response = requests.get(url)

    if response.status_code == 200:
        soup = bs(response.content, 'html.parser')
        div_count = soup.findAll('div', class_='card-visited')
        print('Записано ' + str(len(div_count)) + ' вакансий')
        records = {}
        for div in soup.findAll('div', class_='card-visited'):
            records = {}

            for title in div.findAll('h2', class_='add-bottom-sm'):
                records['title'] = title.text

            for p in div.findAll('p', class_='overflow'):
                records['p'] = p.text

            with open('work_ua_parser.txt', 'a') as fp:
                fp.writelines([records['title'] + records['p'] + '\n' + '='*120 + '\n'*2])

parser(url)

'''-----------------------------------------VERSION_2--------------------------------------------

import requests
from bs4 import BeautifulSoup as bs

url = 'https://ua.jooble.org/%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-django'


def parser(url):
    response = requests.get(url)
    print(response.status_code)
    if response.status_code == 200:
        soup = bs(response.content, 'html.parser')

        for div in soup.find_all('div', class_='result'):
            records = {}
            for link in div.find_all('a'):
                records['link'] = link['href']

                for title in link.find_all('h2'):
                    records['title'] = title.text

            for desc in div.find_all('div', class_='desc'):
                records['desc'] = desc.text

            with open('work_ua_parser.txt', 'a') as fp:
                fp.writelines([records['title'] + records['link'] + records['desc'] + '\n' + '='*120 + '\n'*2])

parser(url)
'''
