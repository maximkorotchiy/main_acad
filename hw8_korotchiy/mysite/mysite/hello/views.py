from django.http import HttpResponse
from django.shortcuts import render

def first_run(request):
    # return HttpResponse('privet')
    return render(request, 'index.html', {'user': request.user})
