from django import forms

class QuadraticEquation(forms.Form):
    a = forms.IntegerField(label=False, widget=forms.NumberInput(attrs={'placeholder': 'a'}))
    b = forms.IntegerField(label=False, widget=forms.NumberInput(attrs={'placeholder': 'b'}))
    c = forms.IntegerField(label=False, widget=forms.NumberInput(attrs={'placeholder': 'c'}))

    def clean_a(self):
        a = self.cleaned_data['a']
        if a == 0:
            raise forms.ValidationError('а не может равняться 0')
        return a

class Square(forms.Form):
    number = forms.IntegerField(label='Число')
    power = forms.IntegerField(label='возвести в', required=False)
