from django.shortcuts import render, get_object_or_404

from post.models import Post

# Create your views here.
def post_list(request):
    context = {'object_list': Post.objects.filter(is_published=True)}
    return render(request, 'list.html', context)


def post_detail(request, pk):
    obj = get_object_or_404(Post, pk=pk)
    context = {'object': obj}
    return render(request, 'detail.html', context)
