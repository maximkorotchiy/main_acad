
from django.urls import path

from post.views import post_list, post_detail


urlpatterns = [
    path('detail/<int:pk>', post_detail, name='post_detail'),
    path('', post_list),
]
