from django import forms

class QuadraticEquation(forms.Form):
    a = forms.IntegerField(label='a')
    b = forms.IntegerField(label='b')
    c = forms.IntegerField(label='c')

    def clean_a(self):
        a = self.cleaned_data['a']
        if a == 0:
            raise forms.ValidationError('а не может равняться 0')
        return a


# ax^2+bx+c=0
# D=b^2-4ac
# x=(-b+-sqrt(D))/2a
