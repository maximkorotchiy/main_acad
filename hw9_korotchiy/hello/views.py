from django.http import HttpResponse
from django.shortcuts import render

from hello.forms import QuadraticEquation

from math import *

def first_run(request):
    if request.POST:
        form2 = QuadraticEquation(request.POST)
        if form2.is_valid():
            a = form2.cleaned_data['a']
            b = form2.cleaned_data['b']
            c = form2.cleaned_data['c']

            D = b**2 - 4*a*c
            x = 0
            x1 = 0
            x2 = 0
            no_x = 0

            if D > 0:
                x1 = (-b + sqrt(D))/(2*a)
                x2 = (-b - sqrt(D))/(2*a)
            elif D == 0:
                x = -b/(2*a)
            else:
                no_x = 'Корней нет'

            return render(request, 'index.html', {
                'form2': QuadraticEquation(), 'success': True, 'D': D, 'x': x, 'x1': x1, 'x2': x2, 'a': a, 'b': b, 'c': c, 'no_x': no_x
            })
        else:
            return render(request, 'index.html', {'form2': form2})
    return render(request, 'index.html', {'form2': QuadraticEquation()})
